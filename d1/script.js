// Introduction to MongoDB

// Data vs Information
/* Data is raw and contains no logical meaning
		(e.g 90%, 50.0)
	information on the otherhand has meaning
		(e.g firstName, lastName)

*/

// Database
	// is manage by database management system (DBMS), just like in a physical library used by librarians (A-Z, catergory) called "Dewey Decimal System"

	// DBMS - is specifically design to manage the storage, retrieval, and modification of data in a database

	// 4 types of Operation /krad/
		// CRUD Operations: Create (insert), Read (Select), Update, and Delete

// Type of Database

	// Relational Database - data is stored as a set of tables with rows and columns (like a table printed on a paper)

	// Unstructured Data - data that cannot fit into a strict tabular format, NoSQL databases are commonly used.

// What is SQL? /sikwel/ or just /S-Q-L/

	// Structured Query Languange - used typically in relational DBMS to store, retrieve and modify data

	/* A code in SQL looks like this:
	
	SELECT id, first_name, last_name FROM students WHERE batch_number = 1;
	
	*/

	// requires tables and information provided in its columns to be defined before they are created and then used to store information

	// because of SQL being complicated, NoSQL databases are becoming popuular due to the flexibility of being able to change the data structure of information in databeses on the fly. and developers can omit the process of recreating tables and backing up data and re-importing them as needed on certain occasions

// What is NoSQL?

	// means Not Only SQL, conceptualized when capturing complex, unstructured data became more difficult (eg. MongoDB)

	// MongoDB - open-source database and leading NoSQL Database. Highly expressive and generally frienldy, and structured just like JSON format

		// Mongo comes from the word "humongous"

	// developer does not need to keep in mind the diff bet writing code for the programitself and data acces in DBMS

	// SOME terms in relational databases will be change: 
	/*
		tables -> collections
		rows -> documents (data that we have)
		columns -> fields (type of data)
	*/

// can NoSQL be trasfered to SQL?

	// NoSQL is unstructured and is it hard to transfer, it may be done manually

// HTTP - hypertext transfer protocol 
	// is an application-layer protocol for transmitting hypermedia documents, such as HTML
	// when do we have s on the end of http?
		// S means Secure - and it will show when the app is connected to the host



//----------------AM Break--------------------//



/* OBJECTIVES
	- put real-world data into a database system by learning to analyzing data structure and realtionship

	- define the importance of creating data models

	- enumerate diff types of rel bet data and models

*/

/* 

// What is a data model

	// describes how data is organized or group in a data base

	// by creating data modes, data will be manged by the database system and the applicatio nto be developed.
*/

/*

// DATA MODELLING SIMULATION SCENARIO

	1. web app willl start w/ a simple log-in procedure. (it is a procedure the most pre-exisiting app have)

	2. there will be two type of users: Admin of the system (one that will make the courses) and customers (one that will view the courses)

	3. information about the admin will be placed in the database beforehand. for the custome, they will register to the app before logging in.

	4. During the registration process, they want to get the name and the contact detaisl of the customer. the process will also ask to the preferred login username and password

	5. admin resposnible in adding the details of the course that they offer. should also be able to update the course if offered or not

	6. customers shall have information on which courses they enrolled to. courses shall also contain a list of customers that erolled toa given course.

	7. customer will enroll to a course and will pay for the courses theybe enrolled to creating a trasanction. the transaction will record the status of the payment and the payment method that the customer used.

	// 3 Major Data Models
		1. Customer and Admin details
		2. Courses
		3. Transactions

	NOTE: usually a Diagram is use to picture out the scenario.

// Data Model Design - the key consideration for the structure of you documents.
	
	======== Embedded Data Model =============
		//if embedded, WE are the one manually adding the needed properties
	Pros:
		- it is easy to add data to a field
		- good for small applications
	Cons:
		- data is too large to be handled in a document
		- hassle if too much properties or (large applications) are needed to be done
		- possible of duplication of data

	{
		name: "ABC company"
		contact: "093483843",
		branches: [
			// called sub-document
			{
				<properties> : <value>
				_id: "001"
				name: "branch1",
				address: "QC",
				contact: "09243474645"
			}
			]
	}
	
	{
		userName: "astonPogi_001"
		contact: {
			mobileNo: "09484738333",
			phoneNo: "9348-444-3334"
			faxNo: "09473647345"
		}
	}


	==== Normalized/reference Data Model =====
		// we connect 2 models using the same properties, using the IDs
	Pros:
		- we can easily retrieve the data because the models are separated
		- good for large applications
	Cons:
		- need deep analysis of connection between data

	//branch model
	{	
		_id: <string>,
		name: <String>,
		address: <string>,
		city: <string>,
		supplier_id: <string>
	}

	//suppliers model
	{
		_id: <string>,
		name: <string>,
		contact: <number>,
		branchL [
			<branch_id>
		]
	}


// Creating the data models
	1. Users : will contain info of both the customer and the admin

	2. identify which fields are needed by each data model

	Course Booking Application
	Data Models

	======Users======
	{
		firstname: <String>,
		lastname: <String>,
		age: <String>,
		gender: <String>,
		email: <String>,
		password: <String>,
		mobileNo: <String>,
		isAdmin: <Boolean>,
		enrollments: [
			{
				courseId: <String>,
				status: <String>,
				enrolledOn: <Date>
			}
		],
		paymentStatus: trasactionId
	}


	=====Courses=====
	{
		_id: ObjectId, // MongoDB Automatically sets the Object ID (hence, usually not included in the model)
		name: <string>,
		description: <String>,
		price: <Number>,
		isAvailable: <Boolean>
		createdOn: <Date>,
		enrollees: [
			{
				userId: <string>,
				enrolledOn: <Date>
			}
		]
	}


	===Transactions===
	{
		studentId: <string>,
		courseId: <string>;
		isPaid: <Boolean>,
		totalAmount: <number>,
		paymentMethod: <string>,
		dateTimeCreated: <date>
	}



// Relationship between Data Models

	1. One-to-one 
		ex. A person can only have one employee ID on a given compny
	2. One-to-many
		ex. A person can have one or more email addresses
	3. Many-to-many
		ex. check notes

*/	